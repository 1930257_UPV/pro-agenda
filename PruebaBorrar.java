import java.util.*;
public class PruebaBorrar
{
   public static void main(String... args){
       String frutas[]={"Mansana","Piña","Pera","Sandia","Melon","Kiwi"};
       List <String>lista = new <String>ArrayList();
       for(int i=0; i<frutas.length;i++){
           lista.add(frutas[i]);
       }
       //para mostrar
       System.out.println("\nElemento de la lista ====> "+lista.toString());
       for(int i=0; i<lista.size(); i++){
           System.out.print("\nlista.get("+i+") ====> "+lista.get(i)+"   ");
       }
       //borrar
       int indice = lista.indexOf("Pera");
       System.out.println("\nIndice de Pera ===> " + lista.indexOf("Pera"));
       
       lista.remove(indice);
       //vuelvo a poner que me muestre
       System.out.println("\nElemento de la lista ====> "+lista.toString());
       for(int i=0; i<lista.size(); i++){
           System.out.print("\nlista.get("+i+") ====> "+lista.get(i)+"   ");
       }
       
       
       
       indice = lista.indexOf("Mansana");
       //Mnasana ==> Manzana
       lista.remove(0);
       //aqui quitamos Mansana y vemos que si se haya borrado
       System.out.println("\nElemento de la lista ====> "+lista.toString());
       //aqui agregamos
       lista.add(0, "Manzana");
       //y mostramos ya corregido
       System.out.println("\nElemento de la lista ====> "+lista.toString());
       for(int i=0; i<lista.size(); i++){
           System.out.print("\nlista.get("+i+") ====> "+lista.get(i)+"   ");
       }
       
       
       
       
    }
}
