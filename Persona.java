import java.util.Date;
import java.io.Serializable;

public class Persona implements Serializable//   Persona.java  TIPO DE DATO REFERENCIADO
{
    String nombre; // Tiene modificador de acceso DEFAULT
    Date fdn; // Fecha De Nacimiento ==> fdn
    
    public Persona(String a, Date b){
        this.nombre = a;
        this.fdn = b;
    }
    
    public Persona(String a){
         this.nombre = a;
    }
    
    public String getNombre(){ // Metodo getter para nombre
          return this.nombre;
    }
    
    public void setNombre(String a){
         this.nombre = a;
    }
    
    public Date getFdn(){ // Metodo getter para fdn
          return this.fdn;
    }
    
    public void setFdn(Date a){
         this.fdn = a;
    }
    
    public boolean equals(Persona a){
        return  a.nombre.equals( nombre ) && a.fdn.equals( fdn );
    }
    
    public String toString(){
        return nombre + " - "/*+ fdn*/;
    }
}